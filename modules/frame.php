<?php

/*

From LARES project: https://gitlab.com/dustwolf/lares/-/blob/master/frames.php

*/

class frame {

 function __construct($context, $naslov = "Neimenovan", $vsebina = "/", $ajax = True, $page = "") {

  $tip = "aktuator"; $ikona = "motor";
  $barva = "gray";

?>
<?php if($page != "") { ?><a href="<?php echo $context->url."index.php/".$page; ?>"><?php } ?>
 <div class="wrapper">
  <div class="icon iconSmall">
   <div class="iconColor iconColorSmall" style="background-color: <?php echo $barva; ?>; " id="<?php echo md5($naslov); ?>-iconColor">
    <div class="iconCore iconCoreSmall" style="background-image: url('<?php echo $context->url."images/".$ikona; ?>.png'); ">
     <img id="<?php echo md5($naslov); ?>-iconSpin" class="iconSpin" src="<?php echo $context->url."images/".$tip; ?>.png" alt="<?php echo $tip; ?>">
    </div>
   </div>
  </div>
  <div class="content contentSmall">
   <b><?php echo $naslov; ?></b><div id="<?php echo md5($naslov); ?>-content"><?php echo $vsebina; ?></div>
  </div>
  <?php if($ajax) {
    ob_start(); ?>
    function refresh_<?php echo md5($naslov); ?>() {
     jQuery.ajax("<?php echo $context->url; ?>status.php?update=<?php echo rawurlencode($naslov); ?>").done(
      function(json) { data = jQuery.parseJSON(json);
       if(typeof(data.html2) != "undefined" && data.html2 !== null) {
        jQuery("#<?php echo md5($naslov); ?>-content").html(data.html+"<br>"+data.html2);
       } else {
        jQuery("#<?php echo md5($naslov); ?>-content").html(data.html);
       }
       jQuery("#<?php echo md5($naslov); ?>-iconColor").css("background-color", data.iconColor);
       window.Active<?php echo md5($naslov); ?> = data.active;
       window.Alert<?php echo md5($naslov); ?> = data.alert;
      }
     );
    }
    refresh_<?php echo md5($naslov); ?>();
    setInterval(refresh_<?php echo md5($naslov); ?>, 5000);
    window.window.angle<?php echo md5($naslov); ?> = 45;
    setInterval(function(){
     if(window.Alert<?php echo md5($naslov); ?>) {
      jQuery("#<?php echo md5($naslov); ?>-iconSpin").css("transition","all 1s linear");
      window.angle<?php echo md5($naslov); ?> = 45;
     } else {
       if(window.Active<?php echo md5($naslov); ?>) {
        jQuery("#<?php echo md5($naslov); ?>-iconSpin").css("transition","all 0s");
        window.angle<?php echo md5($naslov); ?> += 3;
       } else {
        jQuery("#<?php echo md5($naslov); ?>-iconSpin").css("transition","all 2s linear");
        window.angle<?php echo md5($naslov); ?> = 0;
       }
     }
     jQuery("#<?php echo md5($naslov); ?>-iconSpin").rotate(window.angle<?php echo md5($naslov); ?>);
    },50);
  <?php 
  $context->addJS(md5($naslov), ob_get_clean());
  } ?>
 </div>
<?php if($page != "") { ?></a><?php } ?>
<?php
 }

}

?>
