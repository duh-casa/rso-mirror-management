<?php

class stats {

	private $datapath;
	public $data; 
	private $quick;

	/* in quick mode, filesizes are not calculated and stats file is not written */

	function __construct($datapath = "/data/images/", $quick = False) {
		$this->datapath = $datapath;
		$this->quick = $quick;
		$this->data = $this->getStats();
	}

	private function getStats() {
		$out = array();

		foreach(glob($this->datapath."*", GLOB_ONLYDIR) as $dir) {
			if($this->quick) {
				$tmp = array("");
			} else {
				$tmp = array();
				exec("/bin/du -sh ".escapeshellarg($dir). " | cut -d$'\t' -f1", $tmp);
			}

			$out[basename($dir)] = array(
				"mtime" => filemtime($dir),
				"size" => $tmp[0]
			);
		}

		return $out;
	}

	public function readStats($filename = "../images.stats.json") {
		return json_decode(file_get_contents($this->datapath.$filename), true);
	}

	function __destruct() {
		if(!$this->quick) { $this->writeStats($this->data); }
	}

	private function writeStats($out, $filename = "../images.stats.json") {
		file_put_contents(
			$this->datapath.$filename, json_encode($out, JSON_PRETTY_PRINT)
		);
	}

}
