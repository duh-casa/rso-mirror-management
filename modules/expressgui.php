<?php

class expressgui {

	private $datapath;
	private $installList;
	private $descritpions;
	private $runtimes;

	function __construct(
		$datapath = "/data/proins/proins/express-gui/ProIns Loader/DATA/"
	) {
		$this->datapath = $datapath;

		$this->loadInstalList();
		$this->loadDescritpions();
		$this->loadRuntimes();
	}

	private function loadInstalList() {
		$this->installList = file($this->datapath."INSTALL_LIST");
	}

	private function loadDescritpions() {
		foreach($this->installList as $index => $install) {
			$this->descritpions[$index] = 
				file_get_contents($this->datapath.$index.".descritpion");
		}
	}

	private function loadRuntimes() {
		foreach($this->installList as $index => $install) {
			$this->runtimes[$index] = 
				explode(" ", file_get_contents($this->datapath.$index.".runtime"));
		}
	}


	//returns array of indexes if found
	public function runtimeIndex($install = "") {
		$out = array();
		foreach($this->installList as $index => $tmp) {
			if($this->runtimes[$index][2] == $install) {
				$out[] = $index;
			}
		}
		return $out;
	}

	public function runtimeEFI($index) {
		return (stripos($this->runtimes[$index][1], "efi") !== False);
	}

	//$install is the image name
	public function addInstall($install = "", $update = False) {
		$data = $this->loadInstallData($install);

		if($update === False) { $update = time();	}

		$dateAppend = 
			"\n\n\nZadnja posodobitev namestitvene slike: ".date("j. n. Y", $update); 

		$this->installList[] = "Legacy - ".$data["title"];
		$this->descritpions[] = $data["descritpion"].$dateAppend;
		$this->runtimes[] = array(
			"sudo",
			"/proins/scripts-express/supervisor.sh",
			$install,
			"3F2E-0F70"
		);

		$this->installList[] = "UEFI - ".$data["title"];
		$this->descritpions[] = $data["descritpion"].$dateAppend;
		$this->runtimes[] = array(
			"sudo",
			"/proins/scripts-express-efi/supervisor.sh",
			$install,
			"3F2E-0F70"
		);

	}

	public function updateInstall($index, $install, $update = False) {
		$data = $this->loadInstallData($install);

		if($update === False) { $update = time();	}

		$dateAppend = 
			"\n\n\nZadnja posodobitev namestitvene slike: ".date("j. n. Y", $update); 

		if(!$this->runtimeEFI($index)) {

			$this->installList[$index] = "Legacy - ".$data["title"];
			$this->descritpions[$index] = $data["descritpion"].$dateAppend;
			$this->runtimes[$index] = array(
				"sudo",
				"/proins/scripts-express/supervisor.sh",
				$install,
				"3F2E-0F70"
			);

		} else {

			$this->installList[$index] = "UEFI - ".$data["title"];
			$this->descritpions[$index] = $data["descritpion"].$dateAppend;
			$this->runtimes[$index] = array(
				"sudo",
				"/proins/scripts-express-efi/supervisor.sh",
				$install,
				"3F2E-0F70"
			);

		}

	}

	//description is obtained from the image /home/opis.txt
	private function loadInstallData($install = "") {
		if(file_exists("/data/images/".$install."/home/opis.txt")) {
			$data = file("/data/images/".$install."/home/opis.txt");
		} else {
			$data = array($install."\n", "Ni opisa\n");
		}

		$title = array_shift($data);

		return array(
			"title" => $title,
			"descritpion" => implode("", $data)
		);
	}


	function __destruct() {
		$this->saveRuntimes();
		$this->saveDescritpions();
		$this->saveInstallList();
	}

	private function saveInstallList() {
		file_put_contents(
			$this->datapath."INSTALL_LIST",
			implode("", $this->installList)
		);
	}

	private function saveDescritpions() {
		foreach($this->installList as $index => $install) {
			file_put_contents(
				$this->datapath.$index.".descritpion",
				$this->descritpions[$index]
			);
		}
	}

	private function saveRuntimes() {
		foreach($this->installList as $index => $install) {
			file_put_contents(
				$this->datapath.$index.".runtime",
				implode(" ", $this->runtimes[$index])
			);
		}
	}


}
