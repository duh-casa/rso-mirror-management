<?php

class form {

 private $open;
 private $submit;

 function __construct($submit = "Save") {
  $this->submit = $submit;
  ?><form class="form-horizontal" role="form" method="POST"><?php
  $this->open = True;
 }

 function field($label, $id = "", $value = "") {
  if($id == "") {
   $id = str_replace(" ","-",$label);
  }
  ?>
   <div class="form-group">
    <label class="control-label col-sm-2" for="<?php echo $id; ?>"><?php echo $label; ?>:</label>
    <div class="col-sm-10">
     <input type="text" class="form-control" id="<?php echo $id; ?>" value="<?php echo $value; ?>">
    </div>
   </div>
  <?php
 }

 function close() {
  ?>
   <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" class="btn btn-primary"><?php echo $this->submit; ?></button>
   </div>
  </form>
  <?php
 }

 function __destruct() {
  if($this->open) {
   $this->close();
  }
 }


}

?>
