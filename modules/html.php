<?php

/**
 * HTML PAGE CREATION MODULE
 * Contains: Class to create a HTML document
 */

class html {

 public $jQuery;
 public $chosen;
 public $bootstrap;
 public $css;
 public $js;
 public $fontAwesome;
 public $horwheel;
 public $handheldFriendly;
 public $favicon;
 public $scrollTo;
 public $spectrum;
 public $googleFont;
 public $offlineFont;
 public $datepicker;
 public $recaptcha;
 public $jQueryTop;
 public $lightGallery;
 public $analytics;
 public $rotate;

 public $url;

 private $jscache;

 function __construct($naslov = "", $opcije = array()) {
  $this->url = "http".(!empty($_SERVER['HTTPS'])?"s":"")."://".$_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
  if(isset($_GET["file"])) {
   $this->url = substr($this->url, 0, -strlen($_GET["file"]));
  }
  $this->url = substr($this->url, 0, strrpos($this->url, "/")+1);

  if(isset($_SERVER["PATH_INFO"])) {
   $validPos = strpos($this->url, basename($_SERVER["SCRIPT_NAME"]));
   $this->url = substr($this->url, 0, $validPos);
  }

  $this->jQuery = isset($opcije["jQuery"]);
  $this->jQueryTop = isset($opcije["jQueryTop"]);
  $this->chosen = isset($opcije["chosen"]);
  $this->bootstrap = isset($opcije["bootstrap"]);
  $this->fontAwesome = isset($opcije["fontAwesome"]);
  $this->horwheel = isset($opcije["horwheel"]);
  $this->handheldFriendly = isset($opcije["handheldFriendly"]);
  $this->scrollTo = isset($opcije["scrollTo"]);
  $this->spectrum = isset($opcije["spectrum"]);
  if(isset($opcije["googleFont"])) {
   $this->googleFont = $opcije["googleFont"];
  } else {
   $this->googleFont = "";
  }
  if(isset($opcije["offlineFont"])) {
   $this->offlineFont = $opcije["offlineFont"];
  } else {
   $this->offlineFont = array();
  }
  $this->datepicker = isset($opcije["datepicker"]);
  $this->recaptcha = isset($opcije["recaptcha"]);
  $this->lightGallery = isset($opcije["lightGallery"]);
  if(isset($opcije["analytics"])) {
   $this->analytics = $opcije["analytics"];
  } else {
   $this->analytics = False;
  }  
  $this->rotate = isset($opcije["rotate"]);
  
  if(isset($opcije["css"])) {
   $this->css = $opcije["css"];
  } else {
   $this->css = "";
  }
  if(isset($opcije["js"])) {
   $this->js = $opcije["js"];
  } else {
   $this->js = array();
  }
  if(isset($opcije["favicon"])) {
   $this->favicon = $opcije["favicon"];
  } else {
   $this->favicon = "";
  }


  if($this->chosen || $this->bootstrap || $this->js != array() 
  || $this->horwheel || $this->scrollTo || $this->datepicker || $this->lightGallery || $this->rotate) {
   $this->jQuery = True;
  }

  $this->jscache = array();
 
?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <?php if($this->analytics !== False) { ?>  
   <!-- Global Site Tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $this->analytics; ?>"></script>
   <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments)};
     gtag('js', new Date());

     gtag('config', '<?php echo $this->analytics; ?>');
   </script>
  <?php } ?>  
  <?php if($this->handheldFriendly) { ?>
   <meta name="HandheldFriendly" content="True">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <?php } ?>
  <title><?php echo $naslov; ?></title>
  <?php if($this->bootstrap) { ?>
   <link rel="stylesheet" href="<?php echo $this->url; ?>css/bootstrap.min.css"/>
  <?php } ?>
  <?php if($this->chosen) { ?>
   <link rel="stylesheet" href="<?php echo $this->url; ?>css/chosen.min.css">
  <?php } ?>
  <?php if($this->fontAwesome) { ?>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <?php } ?>
  <?php if($this->spectrum) { ?>
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
  <?php } ?>
  <?php if($this->googleFont != "") { ?>
   <link rel="stylesheet" href="">
   <link href="https://fonts.googleapis.com/css?family=<?php echo $this->googleFont; ?>" rel='stylesheet' type='text/css'>
  <?php } ?>
  <?php if(count($this->offlineFont) == 2) { ?>
   <style type="text/css">
    @font-face {
      font-family: '<?php echo $this->offlineFont[0]; ?>';
      font-style: normal;
      font-weight: 400;
      src: url('../fonts/<?php echo $this->offlineFont[1]; ?>.eot'); /* IE9 Compat Modes */
      src: local(''),
           url('../fonts/<?php echo $this->offlineFont[1]; ?>.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
           url('../fonts/<?php echo $this->offlineFont[1]; ?>.woff2') format('woff2'), /* Super Modern Browsers */
           url('../fonts/<?php echo $this->offlineFont[1]; ?>.woff') format('woff'), /* Modern Browsers */
           url('../fonts/<?php echo $this->offlineFont[1]; ?>.ttf') format('truetype'), /* Safari, Android, iOS */
           url('../fonts/<?php echo $this->offlineFont[1]; ?>.svg#OpenSans') format('svg'); /* Legacy iOS */
    }
   </style>
  <?php } ?>
  <?php if($this->jQuery && $this->jQueryTop) { ?>
   <script type="text/javascript" src="<?php echo $this->url; ?>js/jquery-1.12.2.min.js"></script>
  <?php } ?>
  <?php if($this->datepicker) { ?>
   <link rel="stylesheet" href="<?php echo $this->url; ?>modules/datepicker/css/bootstrap-datepicker3.min.css" />
  <?php } ?>
  <?php if($this->recaptcha) { ?>
   <script src='https://www.google.com/recaptcha/api.js'></script>
  <?php } ?>
  <?php if($this->lightGallery) { ?>
   <link rel="stylesheet" href="<?php echo $this->url; ?>modules/lG/css/lightgallery.min.css" />
  <?php } ?>
  <?php if($this->css != "") { ?>
   <link rel="stylesheet" href="<?php echo $this->url.$this->css; ?>">
  <?php } ?>
  <?php if($this->favicon != "") { ?>
   <link rel="shortcut icon" type="image/ico" href="<?php echo $this->url.$this->favicon; ?>"/>
  <?php } ?>
 </head>
 <body>
<?php
 }

 function test() {
  ?>WORKS!<?php
 }

 function addJS ($id = "default", $js = "") {
  $this->jscache[$id] = '<script type="text/javascript">'.$js."</script>";
 }

 function hereJS() {
  echo implode("", $this->jscache);
  $this->jscache = array();
 }

 function __destruct() {
?>
 <?php if($this->jQuery && !$this->jQueryTop) { ?>
  <script type="text/javascript" src="<?php echo $this->url; ?>js/jquery-1.12.2.min.js"></script>
 <?php } ?>
 <?php if($this->lightGallery) { ?>
  <script src="<?php echo $this->url; ?>modules/lG/js/lightgallery.min.js" type="text/javascript"></script>
  <script src="<?php echo $this->url; ?>modules/lG/js/lightgallery-all.min.js" type="text/javascript"></script>
 <?php } ?>
 <?php if($this->datepicker) { ?>
  <script src="<?php echo $this->url; ?>modules/datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="<?php echo $this->url; ?>modules/datepicker/locales/bootstrap-datepicker.sl.min.js" type="text/javascript"></script>
 <?php } ?>
 <?php if($this->bootstrap) { ?>
  <script src="<?php echo $this->url; ?>js/bootstrap.min.js"></script>
 <?php } ?>
 <?php if($this->horwheel) { ?>
  <script type="text/javascript" src="<?php echo $this->url; ?>js/horwheel.js"></script>
  <script>
   var wrapper = document.querySelector('#wrapper');
   horwheel(wrapper);
  </script>
 <?php } ?> 
 <?php if($this->scrollTo) { ?>
  <script src="//cdn.jsdelivr.net/jquery.scrollto/2.1.2/jquery.scrollTo.min.js"></script>
 <?php } ?>
 <?php if($this->spectrum) { ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
 <?php } ?>

 <?php 
  if($this->js != array()) { 
   foreach($this->js as $js) {
    ?><script src="<?php echo $this->url.$js; ?>"></script><?php 
   }
  }
  ?>
 <?php if($this->chosen) { ?>
  <script type="text/javascript" src="<?php echo $this->url; ?>js/chosen.jquery.min.js"></script>
 <?php } ?> 

 <?php if($this->rotate) { ?>
  <script src="<?php echo $this->url; ?>js/jQueryRotateCompressed.js"></script>
 <?php } ?>

 <?php 
  if($this->jscache != array()) {
   $this->hereJS(); 
  }
 ?>
 </body>
</html>
<?php
 }

}

?>
