#!/bin/php
<?php
spl_autoload_register(function ($class_name) { require_once "modules/".$class_name.'.php'; });

$stats = new stats();
$guidata = new expressgui();

//iterate through found installs
foreach($stats->data as $name => $install) {

	//find indexes of matching images
	$indexes = $guidata->runtimeIndex($name);

	//update matching images
	foreach($indexes as $index) {
		$guidata->updateInstall($index, $name, $install["mtime"]);
	}

	//if install not found
	if(count($indexes) == 0) {
		$guidata->addInstall($name, $install["mtime"]);
	}

}
