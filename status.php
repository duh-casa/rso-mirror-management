<?php

spl_autoload_register(function ($class_name) { require_once "modules/".$class_name.'.php'; });

$output = (object) True;

ob_start();

$c = new config();

$output->active = False;
$output->alert = False;
if($_GET["update"] == "Uplink") {

	//run nmcli
	$tmp = array();
	exec('nmcli -t con show '.escapeshellarg($c->s->router_eth0), $tmp);

	//parse output
	$uplink = array();
	foreach($tmp as $line) {
		$splitline = explode(":", $line);
		$uplink[$splitline[0]] = $splitline[1];
	}

	//friendly output
	$output->active = ($uplink["GENERAL.STATE"] == "activated");

	$linkState = trim(file_get_contents("/sys/class/net/".$c->s->router_eth0."/operstate"));
	$output->alert = ($linkState == "down");

	$html = array();
	$html[] = "Link ".$linkState;

	if($output->active) {
		$html[0] .= "; IP: ".$uplink["IP4.ADDRESS[1]"];
		$html[] = "DNS: ".$uplink["IP4.DNS[1]"];
	} else {
		$html[] = "Error ".$uplink["GENERAL.STATE"];
	}

	$output->html = $html[0];
	if(isset($html[1])) { $output->html2 = $html[1];	}

} elseif($_GET["update"] == "eRSO gateway") {

	$tmp = array();
	exec('systemctl show autossh-tunnel --no-page', $tmp);

	//parse output
	$service = array();
	foreach($tmp as $line) {
		$splitline = explode("=", $line);
		$service[$splitline[0]] = $splitline[1];
	}

	//friendly output
	$output->active = ($service["ActiveState"] == "active");

	//tried using journalctl here but it is unreliable
	$tmp = array();
	exec("curl -m 1 -Is http://erso", $tmp);	
	if(count($tmp) == 0) {
		$output->html = "Problem";
		$output->alert = True;
	} else {
		$output->html = "Povezan";
	}

} elseif($_GET["update"] == "Proins mirror") {

	$c = new config();
	$f = "/data/images.mirror.stats";

	if($c->s->management_capabilities->proins_download) {
		$output->active = true;

		if(file_exists($f)) {

			//parse file
			$status = array();
			foreach(file($f) as $line) {
				$sline = explode(":", $line);
				if(isset($sline[1])) {
					$status[trim($sline[0])] = trim($sline[1]);
				}
			}

			//has rsync returned stats?
			if(isset($status["Total file size"])) {
				$output->alert = false;
				$output->html = "Datum: ".date ("Y-m-d H:i:s", filemtime($f));
				$output->html2 = "Velikost: ".((string) round(str_replace(",","",$status["Total file size"]) / 1024 / 1024 / 1024))." GB";
			} else {
				//no stats: error
				$output->alert = true;
				$output->html = "sync error";
			}

		} else {
			//no stats file: error
			$output->alert = true;
			$output->html = "sync error";
		}

		if($output->alert) {
			$tmp = array();
			exec("ps aux | grep rsync | grep data", $tmp);
			array_pop($tmp);
			if(count($tmp) > 0) {
				$output->active = true;
				$output->alert = false;
				$output->html = "Sinhronizacija v teku";
			}			
		}


	} else {
		//not enabled
		$output->html = "disabled";
		$output->active = false;
		$output->alert = false;
	}

} elseif($_GET["update"] == "Sync status") {

	$pids = array();
	exec('pgrep rsync', $pids);
	$output->active = (count($pids) > 1);
	if($output->active) {
		$output->html = "Kopiranje trenutno v teku";
	} else {
		$output->html = "Kopiranje ni aktivno";
	}

	$output->alert = (file_exists("/var/run/mirror_upload_images") || 
		                file_exists("/var/run/mirror_upload_proins") ||
		                file_exists("/var/run/mirror_download"));
	if($output->alert) { $output->html = "Kopiranje je načrtovano";	}

}

//color coding
if($output->active) {
# $output->iconColor = "#2dff5b";  //dela = zelen
 $output->iconColor = "#00ff00";  //dela = zelen
 $output->iconSpin = "iconSpin-1";
} else {
# $output->iconColor = "#ff2d2d";  //ustavljen = rdeč
 $output->iconColor = "#ff0000";  //ustavljen = rdeč
 $output->iconSpin = "iconSpin-0";
}

if($output->alert) {
#	$output->iconColor = "#fffd2d";
 	$output->iconColor = "#ffff00";
  $output->iconSpin = "iconSpin-2";
}

$output->error = ob_get_clean();
echo json_encode($output);