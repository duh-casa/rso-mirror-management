<h1>Izklop strežnika</h1>

<p>Ta vmesnik vam omogoča varen izklop strežnika. Prosimo uporabite to tipko namesto drugih metod.</p>

<?php

#https://stackoverflow.com/a/5826877/2897386
if(!empty($_POST) && isset($_POST["shutdown"])) {
	passthru("sudo poweroff");
}

?>

<div style="text-align: center;"><form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
	<input type="hidden" name="shutdown" value="1">
	<input class="btn btn-danger btn-lg" type="submit" value="Izklop strežnika">
</form></div>
<br>

<p>Izklopa vam <b>ne</b> priporočamo, če niste v bližini strežnika da bi ga po potrebi ponovno vklopili.</p>
