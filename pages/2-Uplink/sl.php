<h1>Nastavitev mreže</h1>

<div class="middle" style="width: 300px;"><?php

new frame($this->context, "Uplink", "Preverjam...");

?></div>

<h2>Spremeni nastavitve</h2>
<?php

	$c = new config();

	if(!empty($_POST) && isset($_POST["uplink_save"])) {

		# Example: nmcli conn modify "Wired connection 1" ipv4.addresses 192.2.0.100/24 ipv4.gateway 192.0.2.254 ipv4.method manual
		if($_POST["uplink_method"] == "manual") {
			passthru("sudo nmcli conn modify ".$c->s->router_eth0.
				" ipv4.address ".escapeshellarg($_POST["uplink_address"]).
				" ipv4.gateway ".escapeshellarg($_POST["uplink_gateway"]).
				" ipv4.dns ".escapeshellarg($_POST["uplink_dns"]).
				" ipv4.method manual"
			);
		} else {
			passthru("sudo nmcli conn modify ".$c->s->router_eth0." ipv4.method auto");
		}

		//apply changes
		?><p class="console"><?php
		passthru("sudo nmcli connection up id ".$c->s->router_eth0);
		?></p><br><?php
		sleep(1);

	}

	$tmp = array();
	exec("nmcli -g ipv4.method connection show ".$c->s->router_eth0, $tmp);
	$method = $tmp[0];

	$tmp = array();
	exec("nmcli -g ip4.address connection show ".$c->s->router_eth0, $tmp);
	$address = trim(explode("|", $tmp[0])[0]); //after pipe are unused settings

	$tmp = array();
	exec("nmcli -g ip4.gateway connection show ".$c->s->router_eth0, $tmp);
	$gateway = trim(explode("|", $tmp[0])[0]);

	$tmp = array();
	exec("nmcli -g ip4.dns connection show ".$c->s->router_eth0, $tmp);
	$dns = trim(explode("|", $tmp[0])[0]);


	?>
	<div style="text-align: center;"><form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<input type="hidden" name="uplink_save" value="1">
		
		<div class="form-check">
			<input class="form-check-input" type="radio" id="uplink_auto" name="uplink_method" value="auto" <?php if($method == "auto") { ?>checked="checked"<?php } ?>>
			<label class="form-check-label" for="uplink_auto">DHCP (avtomatsko)</label>
		</div>

		<div class="form-check">
			<input class="form-check-input" type="radio" id="uplink_manual" name="uplink_method" value="manual" <?php if($method == "manual") { ?>checked="checked"<?php } ?>>
			<label class="form-check-label" for="uplink_manual">statično</label><br>
		</div>

		<label for="uplink_address">IP naslov/subnet:</label>
    <input type="text" class="form-control" id="uplink_address" name="uplink_address" placeholder="1.2.3.4/24" value="<?php echo $address; ?>">

		<label for="uplink_gateway">Gateway:</label>
    <input type="text" class="form-control" id="uplink_gateway" name="uplink_gateway" placeholder="1.2.3.1" value="<?php echo $gateway; ?>">

		<label for="uplink_dns">DNS strežniki (loči z presledki):</label>
    <input type="text" class="form-control" id="uplink_dns" name="uplink_dns" placeholder="8.8.8.8 1.1.1.1" value="<?php echo $dns; ?>">

    <p>Opomba: Ročno vpisane nastavitve se shranijo le v primeru da je izbrana opcija "statično".</p>
    <br>
		<input class="btn btn-success btn-lg" type="submit" value="Shrani">
	</form></div>
	<br>
