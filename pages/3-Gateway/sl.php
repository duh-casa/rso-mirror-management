<h1>eRSO Gateway</h1>

<?php
	//restart before displaying status
	if(!empty($_POST) && isset($_POST["restart_gateway"])) {	
		exec("sudo systemctl restart autossh-tunnel");
		sleep(1);
	}
?>

<div class="middle" style="width: 300px;"><?php

new frame($this->context, "eRSO gateway", "Preverjam...");

?></div>

<h2>Stanje storitve</h2>
<p class="console">
<?php 
	$output = array();
	exec("systemctl status autossh-tunnel", $output);
	echo implode("<br>",$output);
?>
</p>

<h2>Restart storitve</h2>

<div style="text-align: center;"><form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
	<input type="hidden" name="restart_gateway" value="1">
	<input class="btn btn-warning btn-lg" type="submit" value="Ponovno zaženi storitev Gateway">
</form></div>
<br>
<p>Opomba: Storitve ni potrebno ponovno zaganjati. Ko bo zaznala delujoč Internet se bo samodejno (ponovno) vzpostavila. Tipka za restart je za primer da se nekaj zamlomi in zadeva ne deluje več.</p>