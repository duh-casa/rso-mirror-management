<h1>Proins mirror</h1>

<div class="middle" style="width: 300px;"><?php

new frame($this->context, "Proins mirror", "Preverjam...");

?></div>
<p class="console">
<?php
	$output = array();
	exec("ps aux | grep rsync | grep data", $output);
	array_pop($output);
	if(count($output) > 0) {
		?>Procesi sinhronizacije:<br><?php
		echo implode("<br>",$output);
	}
?>
</p>

<h2>Ročna sinhronizacija</h2>
<?php

$c = new config();

if($c->s->management_capabilities->proins_download) {

	#https://stackoverflow.com/a/5826877/2897386
	if(!empty($_POST) && isset($_POST["download"])) {	
		passthru("sudo touch /var/run/mirror_download");
	}

	?>
	<div style="text-align: center;"><form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<input type="hidden" name="download" value="1">
		<input class="btn btn-success btn-lg" type="submit" value="Ročno poženi posodobitev datotek na izpostavi (download)">
	</form></div>
	<br>
	<?php

}

if($c->s->management_capabilities->proins_upload) {

	#https://stackoverflow.com/a/5826877/2897386
	if(!empty($_POST) && isset($_POST["upload_proins"])) {
		passthru("sudo touch /var/run/mirror_upload_proins");
	}
	if(!empty($_POST) && isset($_POST["upload_images"])) {
		passthru("sudo touch /var/run/mirror_upload_images");
	}

	?>
	<div style="text-align: center;"><form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<input type="hidden" name="upload_proins" value="1">
		<input class="btn btn-warning btn-lg" type="submit" value="Ročno poženi posodobitev Proins OS na RSO centrali (upload_proins)">
	</form></div>
	<br>
	<div style="text-align: center;"><form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<input type="hidden" name="upload_images" value="1">
		<input class="btn btn-warning btn-lg" type="submit" value="Ročno poženi posodobitev image-ov na RSO centrali (upload_images)">
	</form></div>
	<?php

}

?>

<p>Opomba: Lahko traja do 5 minut da se prenos dejansko začne.</p>

<div class="middle" style="width: 300px;"><?php

new frame($this->context, "Sync status", "Preverjam...");

?></div>


<h2>Stanje image-ov na tem strežniku:</h2>
<?php
if(file_exists("/data/images.stats.json")) {
	?>
	<table>
		<tr>
			<th>Ime</th>
			<th>Velikost</th>
			<th>Čas spremembe</th>
		</tr>
<?php
	$stats = new stats("/data/images/", True);
	$statsMatch = True;
	foreach($stats->readStats() as $name => $image) {
		//don't show size if change dates don't match current ones
		if($image["mtime"] != $stats->data[$name]["mtime"]) {
			$image["size"] = "";
			$image["mtime"] = $stats->data[$name]["mtime"];
			$statsMatch = False;
		}
?>
		<tr>
			<td><?php echo $name; ?></td>
			<td><?php echo $image["size"]; ?></td>
			<td><?php echo date("Y-m-d H:i:s", $image["mtime"]); ?></td>
		</tr>
<?php		
	}
?>
	</table>
	<?php
	if(!$statsMatch) {
		?><p>Nekateri podatki v zgornji tabeli se še niso posodobili.</p><?php
	}
} else {
	?><p>Podatki še niso na voljo. Osvežijo se na 6 ur.</p><?php
}
?>

<h2>Podrobnosti sinhronizacije image-ov:</h2>

<p>
<?php
if(file_exists("/data/images.mirror.stats")) {
?>
Čas zadnje sinhronizacije: <?php echo date("Y-m-d H:i:s", filemtime("/data/images.mirror.stats")); ?></p><p class="console">
<?php
	echo nl2br(file_get_contents("/data/images.mirror.stats"));
}
?>
</p>

<h2>Podrobnosti sinhronizacije Proins OS:</h2>
<p>
<?php
if(file_exists("/data/proins.mirror.stats")) {
?>
Čas zadnje sinhronizacije: <?php echo date("Y-m-d H:i:s", filemtime("/data/proins.mirror.stats")); ?></p><p class="console">
<?php
	echo nl2br(file_get_contents("/data/proins.mirror.stats"));
}
?>
</p>
