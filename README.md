# RSO Mirror management

PHP-based RSO Mirror management interface.

## Description

Included PHP code provides:
* An overview of server functions mentioned below
* Uplink status and configuration (using nmcli)
* Gateway status and restart (autossh)
* Proins mirror status (and download on demand)
* Proins mirror upload capability (if configured)
* Interface to shut down the server

The interface also displays various status and debugging information.

Please note that this code is only intended to be accessible on the local network of trusted users! Privileged information can be accessed through the interface and root privileges are directly used by the code in several places. Making this interface accessible to untrusted users could result in the server being compromised.

The recommended operating system for running this code is [CentOS 8 Stream](https://www.centos.org/centos-stream/). The code was not tested on other operating systems.

## Installation

This code is deployed using Ansible:
https://gitlab.com/duh-casa/rso-mirror/-/tree/master/roles/management

On deploy a JSON file is generated at `/var/www/html/config.json` containing install-time system settings, such as names of network interfaces, configured functions and similar.

## Usage

After deploying, connect your computer to the LAN interface of your Mirror server and open a browser to port 8080 of the gateway of your chosen subnet. 

For example if your subnet is `192.168.0.0/24` you would browse to:
http://192.168.0.1:8080

You will have to authenticate with your chosen credentials, the default username is `admin` and the default password is `admin`.

## Roadmap

Some of the functions which would make sense, but are not needed for a typical production environment have not been implemented yet.

For example, no checkes are made if commonly used functions like mirror download, have been disabled in the configuration.

## Contributing

This software package is created for the RSO project that operates locally in Slovenia. It is made available in open source in the hopes it will be useful, for other people attempting to do something similar. As such we haven't had any need for outside contributions so far. 

If you wish to contribute please contact the existing contributors.

## Licencse

[Affero GPL v3](LICENSE)

## Project status

This code is being used in a production environment and will receive updates as practical usage requires.

