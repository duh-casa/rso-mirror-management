<?php

spl_autoload_register(function ($class_name) { require_once "modules/".$class_name.'.php'; });

$doc = new html("RSO Mirror server - Nadzorna plošča", array(
 "handheldFriendly" => True,
 "bootstrap" => True,
 "rotate" => True,
 "css" => "css/slog.css",
 "offlineFont" => array("Open Sans", "open-sans-v18-latin-regular"),
));

$strani = new pages($doc);

?>
<div class="body-menu-wrapper">
 <div class="body-menu">
  <div id="logo">
   <a href="<?php echo $doc->url; ?>"><img src="<?php echo $doc->url; ?>images/logo.png" alt="RSO Zrcalo"></a>
  </div>
  <div id="flags">
   <a href="?lang=sl"><img src="<?php echo $doc->url; ?>images/sl.gif" alt="sl"></a>
  </div>
  <?php $strani->menu(); ?>
 </div>
</div>
<?php

?> 
<div class="body-text">
<?php

$strani->content();

?></div><?php 

include "footer.php"; 

?>
